<!-- Top content -->
<div class="top-content">
	<div class="inner-bg" style="padding:0px">
		<div class="container">
			<div class="row">
				<div class='col-lg-4'></div>
				<div class="col-lg-5">
					<div class="form-box" style='margin-bottom: 10%;'>
						<div class="form-top">
							<div class="form-top-left">
								<h3>Login to our site</h3>
								<p>Enter username and password to log on:</p>
							</div>
							<div class="form-top-right">
								<i class="fa fa-lock"></i>
							</div>
						</div>
						<div class="form-bottom" style="color:white;">
							<form role="form" action="j_security_check" method="post" class="login-form">
								<div class="form-group">
									<label for="form-username">Username</label>
									<input required type="email" name="j_username" placeholder="Username..." class="form-username form-control" id="form-username" style = "height: 50px;    margin: 0;    padding: 0 20px;    vertical-align: middle;    background: #fff;    border: 3px solid #fff;    font-family: 'Roboto', sans-serif;    font-size: 16px;    font-weight: 300;    line-height: 50px;    color: #888;    -moz-border-radius: 4px;    -webkit-border-radius: 4px;    border-radius: 4px;    -moz-box-shadow: none;    -webkit-box-shadow: none;    box-shadow: none;    -o-transition: all .3s;    -moz-transition: all .3s;    -webkit-transition: all .3s;    -ms-transition: all .3s;    transition: all .3s;">											
								</div>
								<div class="form-group">
									<label for="form-password">Password</label>
									<input required type="password" name="j_password" placeholder="Password..." class="form-password form-control" id="form-password">
								</div>
								<button style="margin-top:5%;" type="submit" class="btn">Sign in!</button>
							</form>
						</div>
					</div>		            
				</div>
				<div class='col-lg-4'></div>                        
			</div>
			
		</div>
	</div>
</div>  		
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>