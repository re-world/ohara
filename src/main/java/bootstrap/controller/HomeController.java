package bootstrap.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ui.Model;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController extends HttpServlet{
	
	private static final Logger logger = LogManager.getLogger(HomeController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String showPage(Locale locale, Model model) {
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		
		logger.info("<< Info - Welcome home! Time : "+formattedDate);
		logger.warn("<< WARN - Welcome home! Time : "+formattedDate);
		logger.debug("<< DEBUG - Welcome home! Time : "+formattedDate);
		
		return "home";
	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
				/* 
        if(request.isUserInRole("Admin"))
        {
            response.sendRedirect("ChangeFeatures.jsp");
        }
        else if(request.isUserInRole("Manager"))
        {
            response.sendRedirect("ApproveFeatures.jsp");
        }
        else if(request.isUserInRole("Customer"))
        {
                if (request.getSession().getAttribute("customer") == null){
                 HttpSession s = request.getSession();
                String customerEmail = request.getRemoteUser();

                ArrayList<Customer> c = (ArrayList<Customer>)(getServletContext().getAttribute("customers"));
                
                for(int i = 0; i < c.size(); i++)
                {
                    if (c.get(i).getEmail().equals(customerEmail))
                    {
                        s.setAttribute("customer", c.get(i));      
                        break;                                  
                   }
                }
                }
            
            request.getRequestDispatcher("CurrentBooking.do").forward(request, response);            
        }
        else
            response.sendRedirect("home.jsp");
		*/
		logger.info(request);
    }
}