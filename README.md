# Installation

mvn clean install

# Lancement
docker build --progress=plain --no-cache .
docker-compose up

http://localhost:8080/ohara/
