# Stage 1: Build tomcat with maven
# =======================================
FROM maven:3.3-jdk-8-onbuild as builder
COPY . /usr/local/ohara
WORKDIR /usr/local/ohara
RUN mvn -Dmaven.test.skip=true package

# Stage 2: Deploy on tomcat from builder image
# =======================================
FROM tomcat:9.0.8-jre8
COPY --from=builder /usr/local/ohara/target/ohara.war /usr/local/tomcat/webapps/ohara.war
EXPOSE 8080
CMD ["catalina.sh", "run"]